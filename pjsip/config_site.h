#define PJ_CONFIG_ANDROID 1
//#define PJMEDIA_HAS_G722_CODEC 1
// Transaction timeouts, default 500 mS, which yields 408 error.
// Need to set it higher, because I see in the log a 408, and the server response nearly afterward.
#define PJSIP_T1_TIMEOUT 1000 
// for TLS support 
#define PJ_HAS_SSL_SOCK 1 
#define PJMEDIA_SOUND_BUFFER_COUNT 32

#define PJMEDIA_HAS_OPUS_CODEC 1
#define PJMEDIA_HAS_SILK_CODEC 1

//enable IPV6
//#define PJ_HAS_IPV6 1


// #define PJ_CONFIG_MINIMAL_SIZE // Note, this turns off debugging!
#include <pj/config_site_sample.h>
