/*

  Movius

  UtfHelper.hpp
  Utility functions for dealing with UTF-8 issues and 
  The limitations in android with non-BMP (Basic Multilingual Plane).
  
*/

#ifndef Movius_UtfHelper_hpp
#define Movius_UtfHelper_hpp
// Movius
#include "utf8.h"
#define UTF_HELPER_FILE "UtfHelper.hpp"
using namespace std;

// Movius - For hex to/from conversion
string bytes2Hex(char * str, int len);

string hex2String( const string & hex );
/*
 *  Movius - methods to convert to utf-8
 */ 
string toUtf8( const string & str );

string fromUtf8( const string & str );

string filterNonBMP( string & in );

#endif