/*

  Movius

  UtfHelper.cpp
  Utility functions for dealing with UTF-8 issues and 
  The limitations in android with non-BMP (Basic Multilingual Plane).
  
*/

// Movius
#include "utf8.h"

#define UTF_HELPER_FILE "UtfHelper.cpp"
using namespace std;

// Movius - For to / from hex conversion
const char * hexArray = "0123456789ABCDEF";
string bytes2Hex(char * str, int len) {
    string hexChars;
    for ( int j = 0; j < len; j++ ) {
       int v = str[j] & 0xFF;
       hexChars+=hexArray[v >> 4];
       hexChars+=hexArray[v & 0x0F];
    }
    return hexChars;
}

string hex2String( const string & hex ) {
    int end = hex.length();
    string out;
    for(int i=0;i<end;i+=2) {
        char part[3] = {hex[i],hex[i+1],0};
        long charFin = strtol(part,0,16);
        out += (char) charFin;
    }    
    return out;
}
/*
 *  Movius - method to convert to utf-8
 */ 
string toUtf8( const string & str) { 
    string fmt = "toUtf8('"+str+"' len=%d) start";
    PJ_LOG( 4,(UTF_HELPER_FILE, fmt.c_str(), str.length()) );
    string ucoded = hex2String(str);
    fmt = "toUtf8('"+bytes2Hex(const_cast<char*>(ucoded.c_str()), ucoded.length())+"' len=%d) sane?";
    PJ_LOG( 4,(UTF_HELPER_FILE, fmt.c_str(), ucoded.length()) );
    string temp;   
    try {
        utf8::replace_invalid(ucoded.begin(), ucoded.end(), back_inserter(temp));
    } catch(...) {
       PJ_LOG( 4,(UTF_HELPER_FILE, "toUtf8 Caught Exception ") );
    }
    fmt = "toUtf8('"+bytes2Hex(const_cast<char*>(temp.c_str()),temp.length())+"' len=%d) end";
    PJ_LOG( 4,(UTF_HELPER_FILE, fmt.c_str(), temp.length()) );
    return temp;
}

string fromUtf8( const string & str) { 
    string fmt = "fromUtf8('"+str+"' len=%d) start";
    PJ_LOG( 4,(UTF_HELPER_FILE, fmt.c_str(), str.length()) );    
    string temp;   
    try {
        utf8::replace_invalid(str.begin(), str.end(), back_inserter(temp));
    } catch(...) {
       PJ_LOG( 4,(UTF_HELPER_FILE, "toUtf8 Caught Exception ") );
    }
    temp = bytes2Hex(const_cast<char*>(temp.c_str()),temp.length());
    fmt = "fromUtf8('"+temp+"' len=%d) end";
    PJ_LOG( 4,(UTF_HELPER_FILE, fmt.c_str(), temp.length()) );
    return temp;
}

string filterNonBMP( string & in ) {
    string out = "";
    string::iterator start = in.begin();
    string::iterator end = in.end();
    while(start!=end) {
        int cp =  0;
        try {
            cp = utf8::next(start, end);
        }
        catch(...) {
            out.append("[INVALID-UTF8]");
            break;
        }
        if(cp > 0xFFFF) {            
            out.append("?");
        }
        else {
            utf8::append(cp,std::back_inserter(out));
        }
    }
    return out;
}


